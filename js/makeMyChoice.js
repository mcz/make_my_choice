
var nbChoice = 0;
var nTitle = 0;
var titleAnimation = ["MAKE", "MY", "CHOICE", "!!!!", "LET", "THE", "COMPUTER", "CHOOSE", "FOR", "YOU", "..."];

var hideResultTitle = document.getElementById("resultTitle");
var allChoices = document.getElementById("content").getElementsByTagName("div");

window.onload = animTiltle();

//-------------------------------------------------------------------- __________ --------------------------------------------------------------------\\
function createNewChoice(nb) {
	var newChoice   = document.createElement("div");
	var textField   = document.createElement("input");
	var buttonAdd   = document.createElement("button");
	var buttonClear = document.createElement("button");

	newChoice.setAttribute("id","choice" + nb);

	buttonAdd.textContent = "+";
	buttonAdd.setAttribute("class", "buttonAdd");
	buttonAdd.addEventListener("click", function(e){
		addChoice();
	});

	buttonClear.textContent = "-";
	buttonClear.setAttribute("class", "buttonDelete");
	buttonClear.setAttribute("id", "clear" + nb);
	buttonClear.addEventListener("click", function(e){
		if(allChoices.length > 1){
			deleteChoice(e);
		}
	});

	newChoice.appendChild(textField);
	newChoice.appendChild(buttonAdd);
	newChoice.appendChild(buttonClear);
	document.getElementById("content").appendChild(newChoice);	
}

function addChoice() {
	nbChoice += 1;
	createNewChoice(nbChoice);
	totalChoices();
}

function deleteChoice(e){
	var num = '';
	for(var i = 5; i < e.target.id.length; i++){
		num += e.target.id[i]; 
	}
	var myNode = document.getElementById("content");
	myNode.removeChild(document.getElementById("choice" + num));
	totalChoices();
}

function totalChoices(){
	document.getElementById("infoList").textContent = "the list contain " + allChoices.length + " choices";
}

//-------------------------------------------------------------------- MAIN START --------------------------------------------------------------------\\
//-- Init the page with 2 choices
for(var i = 0; i < 2; i++){
	createNewChoice(nbChoice);
	nbChoice += 1;
	//hideResultTitle.style.visibility = "hidden";
	totalChoices();
}

//-- Show the result when the user clic the button start
document.getElementById("makeMyChoice").addEventListener("click", function(e){
	changeBackgroundResult();
	var randomNumber = Math.floor((Math.random() * allChoices.length));
	var getResult = allChoices[randomNumber].firstChild.value;
	document.getElementById("result").textContent = getResult;
})

//-- Animate the title --\\
function changeWord(){
	nTitle ++;
	if (nTitle > titleAnimation.length - 1){
		nTitle = 0;
	}
	var wordTitle = titleAnimation[ nTitle ];
	return wordTitle;
}

//-- Change the background color for welcome and result
function randomHue(){
	var randomHue = Math.floor((Math.random() * 360));
	return randomHue;
}

function changeBackground(){
	//console.log("randomHue : " + randomHue);
	var backColo = document.getElementById("welcome");
	backColo.style.backgroundColor = "hsl(" + randomHue() + ", 50%, 60%";
}

function changeBackgroundResult(){
	var backColoResult = document.getElementById("resultSection");
	backColoResult.style.backgroundColor = "hsl(" + randomHue() + ", 50%, 60%"; 
}

//-- Anim the title of welcome
function animTiltle() {
	setInterval(function(){ document.getElementById("titleChoice").innerHTML = changeWord();changeBackground(); }, 800);
}

function restart(){
	location.reload();
}

//-- Show info
document.getElementById("about").addEventListener("click", function(){
	var infoBox = document.getElementById("info");
	infoBox.style.visibility = "visible";
});

//-- Hide info
document.getElementById("closeInfo").addEventListener("click", function(){
	var infoBox = document.getElementById("info");
	infoBox.style.visibility = "hidden";
	
});



